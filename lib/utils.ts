export const createFileTree = (files) => {
  const root = {};

  files.forEach((file) => {
    const pathParts = file.path.split("/");
    let currentNode = root;
    pathParts.forEach((part, index) => {
      if (!currentNode[part]) {
        currentNode[part] = {};
      }
      // If it's the last part of the path, add the file data
      if (index === pathParts.length - 1) {
        currentNode[part] = file;
      } else {
        currentNode = currentNode[part];
      }
    });
  });

  return root;
};

export const sortTree = (node, level = 0) => {
  const sortedKeys = Object.keys(node).sort();
  const sortedNode = {};
  for (let key of sortedKeys) {
    if (typeof node[key] === "object" && node[key].path === undefined) {
      sortedNode[key] = sortTree(node[key], level + 1);
    } else {
      node[key].level = level;
      sortedNode[key] = node[key];
    }
  }
  return sortedNode;
};
