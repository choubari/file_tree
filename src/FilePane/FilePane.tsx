import React from "react";
import { Box, Typography } from "@mui/material";
import { FileRow } from "./components/FileRow";
import { useWorkspaceContext } from "../Workspace/Workspace.context";
import { createFileTree, sortTree } from "../../lib/utils";

export const FilePane = () => {
  const { files } = useWorkspaceContext();
  const fileTree = sortTree(createFileTree(files));

  const renderFileTree = (node, path = [], level = 0) => {
    return Object.keys(node).map((key) => {
      const item = node[key];
      const currentPath = [...path, key];
      if (typeof item === "object" && item.path === undefined) {
        // a directory
        return (
          <React.Fragment key={key}>
            <FileRow file={{ path: currentPath.join("/") }} level={level} />
            {renderFileTree(item, currentPath, level + 1)}
          </React.Fragment>
        );
      } else {
        // a file
        return <FileRow key={item.path} file={item} level={level} />;
      }
    });
  };

  return (
    <Box>
      <Box p={1}>
        <Typography variant="h6">Files</Typography>
      </Box>
      <Box>
        {renderFileTree(fileTree)}
        {/* {files.map((file) => (
          <FileRow key={file.path} file={file} />
        ))} */}
      </Box>
    </Box>
  );
};
