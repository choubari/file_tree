import React from "react";
import { Box, Typography } from "@mui/material";
import { FileIcon } from "./FileIcon";
import { useWorkspaceContext } from "../../Workspace/Workspace.context";

interface IFileRowProps {
  file: File;
  level?: number;
}

export const FileRow = ({ file, level = 0 }) => {
  const { activeFile, activateFile } = useWorkspaceContext();
  const pathStrSplit = file.path.split("/");
  const fileName = pathStrSplit.pop();
  // const directoryPath = pathStrSplit.join("/");
  const isDirectory = !file.contents;

  const handleClick = isDirectory ? undefined : () => activateFile(file.path);

  return (
    <Box
      display="flex"
      height="1.5rem"
      flexDirection="row"
      alignItems="center"
      key={fileName}
      px={1 + level}
      sx={{
        cursor: isDirectory ? "default" : "pointer",
        background: activeFile === file ? "#DADADA" : "inherit",
        "&:hover": {
          background: isDirectory ? "inherit" : "#E6E6E6",
        },
      }}
      onClick={handleClick}
    >
      <Box width="1.5rem">
        <FileIcon fileName={fileName} />
      </Box>
      <Typography variant="body2">{fileName}</Typography>
    </Box>
  );
};
